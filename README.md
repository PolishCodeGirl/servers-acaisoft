# Solution for the recruitment

You can find the description here https://bitbucket.org/acaisoft/recruitment-reactjs/src/master/

### Requirements

UI was created using Node.js v10.6.0. Other versions may or may not work correctly.

### Installation

In `./ui` run:

```shell
npm install
```

### Run

In `./ui` run:

```shell
npm start
```

### Requirements

Our API was created using Node.js v10.6.0. Other versions may or may not work correctly.

### Installation

In `./api` run:

```shell
npm install
```

### Run

In `./api` run:

```shell
npm start
```
