import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: 'Open Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    box-sizing: border-box;
    background-color: #F2F3F6;
  }
  
  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }

  #root {
      display: flex;
      flex-direction: column;
      align-items: center;
  }

  p {
   margin: 0;
  }
`;
