import axios from 'axios';

import updateStatus from '../utils/updateStatus';

const LOAD_SERVERS = 'LOAD_SERVERS';
const TURN_ON_SERVER = 'TURN_ON_SERVER';
const TURN_OFF_SERVER = 'TURN_OFF_SERVER';
const REBOOTING_SERVER = 'REBOOTING_SERVER';
const UPDATE_SERVER = 'UPDATE_SERVER';
const SEARCHED_SERVERS = 'SEARCHED_SERVERS';
const CLEAR_SEARCHED_SERVERS = 'CLEAR_SEARCHED_SERVERS';
const SHOW_ERRORS = 'SHOW_ERRORS';
const CLEAR_ERRORS = 'CLEAR_ERRORS';

const initialState = {
  servers: [],
  searchedServer: '',
  error: false,
};

// Get servers
export const getServers = () => (dispatch, getState) => {
  if (getState().error) {
    dispatch({ type: CLEAR_ERRORS });
  }

  axios
    .get('http://localhost:4454/servers')
    .then((response) => {
      dispatch({
        type: LOAD_SERVERS,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({ type: SHOW_ERRORS });
      console.log(error);
    });
};

export const updateServer = (data) => ({
  type: UPDATE_SERVER,
  payload: data,
});

// Trun on server
export const turnOnServer = (serverId) => (dispatch, getState) => {
  if (getState().error) {
    console.log('Works!!!!');
    dispatch({ type: CLEAR_ERRORS });
  }

  axios
    .put(`http://localhost:4454/servers/${serverId}/on`, { status: 'ONLINE' })
    .then((response) => {
      dispatch({
        type: TURN_ON_SERVER,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({ type: SHOW_ERRORS });
      console.log(error);
    });
};

// Turn off server
export const turnOffServer = (serverId) => (dispatch, getState) => {
  if (getState().error) {
    dispatch({ type: CLEAR_ERRORS });
  }

  axios
    .put(`http://localhost:4454/servers/${serverId}/off`, { status: 'OFFLINE' })
    .then((response) => {
      dispatch({
        type: TURN_OFF_SERVER,
        payload: response.data,
      });
    })
    .catch((error) => {
      dispatch({ type: SHOW_ERRORS });
      console.log(error);
    });
};

// Reboot server
export const rebootServer = (serverId) => (dispatch, getState) => {
  if (getState().error) {
    dispatch({ type: CLEAR_ERRORS });
  }

  axios
    .put(`http://localhost:4454/servers/${serverId}/reboot`, { status: 'REBOOTING' })
    .then((response) => {
      dispatch({
        type: REBOOTING_SERVER,
        payload: response.data,
      });

      setTimeout(() => dispatch(fetchStatuses(serverId)), 1000);
    })
    .catch((error) => {
      dispatch({ type: SHOW_ERRORS });
      console.log(error);
    });
};

// Get the name of the searched servers
export const getSearchedServers = (serverName) => ({
  type: SEARCHED_SERVERS,
  payload: serverName,
});

export const clearSearchedServers = () => ({
  type: CLEAR_SEARCHED_SERVERS,
});

const fetchStatuses = (serverId) => (dispatch) => {
  axios
    .get(`http://localhost:4454/servers/${serverId}`)
    .then((response) => {
      if (response.data.status !== 'REBOOTING') {
        dispatch(updateServer(response.data));
      } else {
        setTimeout(() => dispatch(fetchStatuses(serverId)), 1000);
      }
    })
    .catch((error) => {
      dispatch({ type: SHOW_ERRORS });
      console.log(error);
    });
};

const servers = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_SERVERS:
      return { ...state, servers: action.payload };
    case TURN_ON_SERVER:
    case TURN_OFF_SERVER:
    case REBOOTING_SERVER:
    case UPDATE_SERVER:
      return { ...state, servers: updateStatus(action.payload, state.servers) };
    case SEARCHED_SERVERS:
      return { ...state, searchedServer: action.payload };
    case CLEAR_SEARCHED_SERVERS:
      return { ...state, searchedServer: '' };
    case SHOW_ERRORS:
      return { ...state, error: true };
    case CLEAR_ERRORS:
      return { ...state, error: false };
    default:
      return state;
  }
};

export default servers;
