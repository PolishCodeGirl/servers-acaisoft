import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import servers from './reducers/servers';

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));

const store = createStore(servers, enhancer);

export const { dispatch, getState } = store;

export default store;
