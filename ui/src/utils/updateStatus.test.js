import updateStatus from './updateStatus';

describe('updateStatus', () => {
  const users = [
    { id: 1, name: 'Carlos' },
    { id: 2, name: 'Sofia' },
    { id: 3, name: 'John' },
    { id: 4, name: 'Mela' },
    { id: 5, name: 'Paul' },
  ];

  it('Returns updated array of users', () => {
    const dataToUpdate = { id: 5, name: 'Marzenka' };
    const updatedUsers = [
      { id: 1, name: 'Carlos' },
      { id: 2, name: 'Sofia' },
      { id: 3, name: 'John' },
      { id: 4, name: 'Mela' },
      { id: 5, name: 'Marzenka' },
    ];

    expect(updateStatus(dataToUpdate, users)).toEqual(updatedUsers);
  });

  it('Returns no changed array of users when id of changed data does not exist', () => {
    const dataToUpdate = { id: 6, name: 'Marzenka' };
    const updatedUsers = [
      { id: 1, name: 'Carlos' },
      { id: 2, name: 'Sofia' },
      { id: 3, name: 'John' },
      { id: 4, name: 'Mela' },
      { id: 5, name: 'Paul' },
    ];

    expect(updateStatus(dataToUpdate, users)).toEqual(updatedUsers);
  });

  it('Returns no changed array of users when data to change is an empty object', () => {
    const dataToUpdate = {};
    const updatedUsers = [
      { id: 1, name: 'Carlos' },
      { id: 2, name: 'Sofia' },
      { id: 3, name: 'John' },
      { id: 4, name: 'Mela' },
      { id: 5, name: 'Paul' },
    ];

    expect(updateStatus(dataToUpdate, users)).toEqual(updatedUsers);
  });
});
