export default function updateStatus(data = {}, dataToChange) {
  const output = dataToChange.map((element) => {
    if (element.id === data.id) return data;
    return element;
  });

  return output;
}
