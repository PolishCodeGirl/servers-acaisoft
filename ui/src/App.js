import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import 'styled-components/macro';

import Div from 'styled-kit/Div';

import { getServers } from './reducers/servers';
import { dispatch } from './store';
import ServersList from './components/ServersList';
import Search from './components/Search';
import Loader from './components/Loader';
import InfoBox from './components/InfoBox';

const mapStateToProps = ({ servers, searchedServer, error }) => ({ servers, searchedServer, error });

const propTypes = {
  servers: PropTypes.arrayOf(PropTypes.object),
  searchedServer: PropTypes.string,
  error: PropTypes.bool,
};

function App({ servers, searchedServer, error }) {
  const dataToDisplay = servers.filter((server) => server.name.toLowerCase().includes(searchedServer.toLowerCase()));
  const displayedElements = dataToDisplay.length;

  useEffect(() => {
    dispatch(getServers());
  }, []);

  return (
    <Div column width="100%" maxWidth={1106} padding={10} pTop={38}>
      {error && <InfoBox message="Something went wrong" type="error" />}
      {servers.length > 0 ? (
        <>
          <Div justifyBetween itemsEnd css="color: #494e61">
            <div>
              <h2 css="font-size: 21px; margin: 0;">Servers</h2>
              <p css="font-size: 15px; margin-top: 4px;">Number of elements {displayedElements}</p>
            </div>
            <Search />
          </Div>

          {displayedElements ? (
            <ServersList servers={dataToDisplay} css="margin-top: 14px;" />
          ) : (
            <InfoBox message={`No servers found under the name ${searchedServer}`} css="margin-top: 14px;" />
          )}
        </>
      ) : (
        <Loader />
      )}
    </Div>
  );
}

App.propTypes = propTypes;

export default connect(mapStateToProps)(App);
