import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { dispatch } from '../store';
import { turnOnServer, turnOffServer, rebootServer } from '../reducers/servers';

import useOnClickOutside from '../hooks/useOnClickOutside';

import Div from 'styled-kit/Div';

const propTypes = {
  status: PropTypes.oneOf(['ONLINE', 'OFFLINE', 'REBOOTING']).isRequired,
  id: PropTypes.number.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onStatusClick: PropTypes.func.isRequired,
  onClickOutside: PropTypes.func.isRequired,
};

const StatusChanger = ({ status, id, isOpen, onStatusClick, onClickOutside }) => {
  const statusChangerRef = useRef();

  const handleTurnOn = () => {
    dispatch(turnOnServer(id));
    onStatusClick();
  };

  const handleTurnoff = () => {
    dispatch(turnOffServer(id));
    onStatusClick();
  };

  const handleReboot = () => {
    dispatch(rebootServer(id));
    onStatusClick();
  };

  useOnClickOutside(statusChangerRef, onClickOutside);

  return (
    <Wrapper absolute column isOpen={isOpen} ref={statusChangerRef}>
      {status === 'ONLINE' && (
        <>
          <Action onClick={handleTurnoff}>Turn off</Action>
          <Action onClick={handleReboot}>Reboot</Action>
        </>
      )}

      {status === 'OFFLINE' && <Action onClick={handleTurnOn}>Turn on</Action>}

      {status === 'REBOOTING' && <Action>Pending...</Action>}
    </Wrapper>
  );
};

StatusChanger.propTypes = propTypes;

export default StatusChanger;

const Wrapper = styled(Div)`
  display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
  width: 137px;
  text-align: left;

  background-color: white;
  -webkit-box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.3);

  top: -3px;
  right: 0;
  z-index: 2;
`;

const Action = styled(Div)`
  height: 55px;
  padding-left: 24px;
  align-items: center;

  &:hover {
    background-color: #f2f3f6;
  }
`;
