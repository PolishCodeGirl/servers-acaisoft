import styled from 'styled-components';

const Loader = styled.div`
  border: 16px solid #fff;
  border-top-color: #494e61;
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;

  margin: 10px auto 0;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

export default Loader;
