import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

import StatusChanger from './StatusChanger';

const propTypes = {
  name: PropTypes.string.isRequired,
  status: PropTypes.oneOf(['ONLINE', 'OFFLINE', 'REBOOTING']).isRequired,
  id: PropTypes.number.isRequired,
};

const ServersTile = ({ name, status, id }) => {
  const [isStatusChangerOpen, setIsStatusChangerOpen] = useState(false);

  const handleClick = () => {
    setIsStatusChangerOpen((state) => !state);
  };

  const handleOnClickOutside = () => {
    setIsStatusChangerOpen(false);
  };

  return (
    <tr>
      <td>{name}</td>
      <Status status={status}>{status}</Status>
      <td css="position: relative;">
        <Dots onClick={handleClick}>•••</Dots>
        <StatusChanger
          status={status}
          id={id}
          isOpen={isStatusChangerOpen}
          onStatusClick={handleClick}
          onClickOutside={handleOnClickOutside}
        />
      </td>
    </tr>
  );
};

ServersTile.propTypes = propTypes;

export default ServersTile;

const Status = styled.td`
  ${({ status }) => {
    if (status === 'ONLINE') {
      return `
                color: #33CAD4;
                ::before {
                    content: '● '
                };
            `;
    } else if (status === 'OFFLINE') {
      return `
                color: black;
                ::before {
                    content: '✕ ';
                    color: #EA5885;
                    font-weight: bold;
                };
            `;
    } else if (status === 'REBOOTING') {
      return `
                color: #9CA7D3;
                ::after {
                    content: '...';
                };
            `;
    }
  }}
`;

const Dots = styled.span`
  color: #9ca7d3;
  font-size: 16px;

  display: inline-block;
  width: 37px;
  height: 37px;
  line-height: 37px;

  &:hover {
    background-color: #f2f3f6;
    border-radius: 50%;
  }
`;
