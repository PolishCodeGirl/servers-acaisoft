import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

import ServersTile from './ServerTile';

const propTypes = {
  servers: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const ServersList = ({ servers, ...props }) => {
  return (
    <Wrapper {...props}>
      <thead>
        <tr>
          <th css="width: 30%">NAME</th>
          <th>STATUS</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {servers.map((server) => (
          <ServersTile name={server.name} status={server.status} id={server.id} key={server.id} />
        ))}
      </tbody>
    </Wrapper>
  );
};

ServersList.propTypes = propTypes;

export default ServersList;

const Wrapper = styled.table`
  background-color: white;
  font-size: 13px;

  border-collapse: collapse;
  border: 2px solid #ededf0;

  tr {
    border: 2px solid #ededf0;
  }

  th {
    color: #9ca7d3;
    font-size: 14px;
    text-align: left;
  }

  td,
  th {
    padding: 24px 0 24px 36px;

    &:last-child {
      padding: 0;
      text-align: center;
      width: 100px;
    }
  }
`;
