import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { dispatch } from '../store';
import { getSearchedServers, clearSearchedServers } from '../reducers/servers';

import DebouncedInput from './DebouncedInput';

const mapStateToProps = ({ searchedServer }) => ({ searchedServer });

const propTypes = {
  searchedServer: PropTypes.string,
};

const SearchUsersInput = ({ searchedServer }) => {
  const handleSearch = (serverName) => {
    if (serverName) dispatch(getSearchedServers(serverName));
    else dispatch(clearSearchedServers());
  };

  return <DebouncedInput placeholder="Search" value={searchedServer} onChange={handleSearch} css="flex: 1;" />;
};

SearchUsersInput.propTypes = propTypes;

export default connect(mapStateToProps)(SearchUsersInput);
