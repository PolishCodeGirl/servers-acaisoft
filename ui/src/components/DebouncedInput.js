import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import useDebounce from '../hooks/useDebounce';

const propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  debounceTime: PropTypes.number,
};

const DebouncedInput = ({ value = '', onChange = () => {}, debounceTime = 500, ...props }) => {
  const [internalValue, setInternalValue] = useState(value);
  const debouncedValue = useDebounce(internalValue, debounceTime);

  function handleChange(event) {
    setInternalValue(event.target.value);
  }

  useEffect(() => {
    if (debouncedValue !== value) onChange(debouncedValue);
  }, [debouncedValue, onChange, value]);

  return (
    <Wrapper>
      <i className="fas fa-search" />
      <input value={internalValue} onChange={handleChange} {...props} />
    </Wrapper>
  );
};

DebouncedInput.propTypes = propTypes;

export default DebouncedInput;

const Wrapper = styled.div`
  position: relative;
  color: #a9aec1;

  max-width: 263px;
  width: 100%;

  input {
    padding: 0 16px 0 32px;
    background: #f2f3f6;
    border: 2px solid #d4d7e1;
    border-radius: 50px;
    outline: none;

    height: 38px;
    width: 100%;
  }

  .fa-search {
    position: absolute;
    top: 10px;
    left: 12px;
  }
`;
